package it.unibo.oop.lab.lambda.ex02;

import static org.junit.jupiter.api.DynamicTest.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public final class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
    	
    	List<String> ordSong = new ArrayList<>(); 
    	songs.stream().forEach(a -> ordSong.add(a.getSongName()));
    	ordSong.sort((o1, o2) -> o1.toString().compareTo(o2.toString()));
    	return ordSong.stream();
    }
    
    @Override
    public Stream<String> albumNames() {
        
    	return albums.keySet().stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
    	Set<String> albumOfTheYear = new HashSet<>();
         albums.entrySet().stream().filter(a -> a.getValue() == year).forEach(a -> albumOfTheYear.add(a.getKey()));
         return albumOfTheYear.stream();
    }

    @Override
    public int countSongs(final String albumName) {
    	return (int) (songs.stream().filter(s -> s.getAlbumName().equals(Optional.ofNullable(albumName))).count());
    }

    @Override
    public int countSongsInNoAlbum() {
        return countSongs(null);
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        
    	return songs.stream().
    		filter(a -> a.getAlbumName()
    				.equals(Optional.ofNullable(albumName)))
    				.mapToDouble(s -> s.getDuration()).average();
    	
    }

    @Override
    public Optional<String> longestSong() {
    	 return songs.stream()
    			 .max((o1, o2) -> Double.compare(o1.getDuration(), o2.getDuration())).map(s -> s.getSongName());
    
    }

    @Override
    public Optional<String> longestAlbum() {
      Map<String, Double> lAlbum = new HashMap<>();
    
      songs.stream().filter(a -> a.getAlbumName().isPresent()).forEach(s -> lAlbum
    		  .merge(s.getAlbumName().get(),  lAlbum.getOrDefault(s.getAlbumName().get(), s.getDuration()), (d1, d2) -> {
    			  d1 += d2;
    			  return d1;
    		  }));
     return Optional.ofNullable(lAlbum.entrySet().stream().max((o1, o2) -> Double.compare(o1.getValue(), o2.getValue())).get().getKey());

    }
    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
